var db=require("./fireStore").getDB()
var firebase = require("./fireStore").getFirebase()
var hasCurrentUser = require("./users").hasCurrentUser
var detachListener =function(){}
var updateBookmark =function(){}

firebase.auth().onAuthStateChanged(function(user) {
    
    if (user) {
        detachListener = db.collection("users").doc(user.uid).collection("bookmark").onSnapshot(function(querySnapshot){
            /*
            querySnapshot.forEach(function(doc) {
                console.log("onSnapshot:"+doc.data().status)
            });
            */
            querySnapshot.docChanges.forEach(function(change) {
                if (change.type === "added") {
                    console.log("snapshot added: ", change.doc.data());
                }
                if (change.type === "modified") {
                    console.log("snapshot modified: ", change.doc.data());
                }
                if (change.type === "removed") {
                    console.log("snapshot removed: ", change.doc.data());
                }
            });
        })
        
        updateBookmark = db.collection("users").doc(firebase.auth().currentUser.uid).collection("bookmark").get().then(function(querySnapshot){
        
            querySnapshot.forEach(function(doc) {
    
                db.collection("records").doc(doc.data().bookId).get().then(function(rdoc){
                    
                    //console.log(rdoc.data())
                    var bookmark = {}
                	if(rdoc.exists && rdoc.data().deadline > Date.now()){
                        bookmark={
                		    "bookId":doc.data().bookId,
                            "status":"borrowed",
                            "deadline":rdoc.data().deadline
                		}
                	}
                	else{
                        bookmark={
                		    "bookId":doc.data().bookId,
                            "status":"available",
                            "deadline":null
                		}
                	}
                	return bookmark
                	
                }).then(function(bookmark){
                    db.collection("users").doc(firebase.auth().currentUser.uid).collection("bookmark").doc(doc.data().bookId).set(bookmark)
                })
            });
        })
    
    
    } else {
        detachListener()
    }
});






//server.get("/bookmark",getBookmark)-----------------------------------
exports.getBookmark=function(req,res,next){
    //var userId = req.params.userId
    var bookmarkItems = []
    var user = firebase.auth().currentUser
    
    if(!hasCurrentUser(user,res)) return
    
    updateBookmark.then(function(){
        db.collection("users").doc(user.uid).collection("bookmark").get().then(function(querySnapshot) {
            querySnapshot.forEach(function(doc) {
                	bookmarkItems.push(doc.data())
            });
            res.send(bookmarkItems)
        });
    })
  

}



//server.post("/bookmark",addBookmark)-------------------------------------
exports.addBookmark=function(req,res,next){
    var user = firebase.auth().currentUser
    var bookId = req.body.bookId
    var result={}
    var bookmark={}
    
    if(!hasCurrentUser(user,res)) return
    
    db.collection("records").doc(bookId).get().then(function(doc){
        if(doc.exists && doc.data().deadline > Date.now()){
    		bookmark={
    		    "bookId":bookId,
                "status":"borrowed",
                "deadline":doc.data().deadline
    		}
    	}
    	else{
    	    bookmark={
    		    "bookId":bookId,
                "status":"available",
    		}
    	}
        	
        db.collection("users").doc(user.uid).collection("bookmark").doc(bookId).set(bookmark).then(function(){
            result = {"result":"success"}
            res.send(result)
        }).catch(function(error){
            result = {"result":"fail","error":error}
            res.send(result)
        })
    	
    })
    
    
}


//server.del("/bookmark",removeBookmark)-------------------------------------
exports.removeBookmark=function(req,res,next){
    var user = firebase.auth().currentUser
    var bookId = req.body.bookId
    var result={}
    console.log(bookId)
    if(!hasCurrentUser(user,res)) return
    
    db.collection("users").doc(user.uid).collection("bookmark").doc(bookId).delete()
    .then(function(){
        result = {"result":"success"}
        res.send(result)
    }).catch(function(error){
        result = {"result":"fail","error":error}
        res.send(result)
    })
    
}

