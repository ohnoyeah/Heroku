var db = require("./fireStore").getDB()
var firebase = require("./fireStore").getFirebase()

//server.post("/login",validate)-------------------------------
exports.validate=function(req,res,next){
    var email = req.body.email
    var inputPwd = req.body.password
    var user = {"null":true}
    
    /*
    db.collection("users")
    .doc(email)
    .get()
    .then(function(doc) {
        if(doc.exists && inputPwd == doc.data().password){
            user = doc.data()
            console.log("Login success");
        }
        else{
            console.log("Login fail");
        }
        res.send(user)
    })
    .catch(function(error) {
        console.log("Error getting documents: ", error);
    });
    */
    
    firebase.auth().signInWithEmailAndPassword(email, inputPwd)
    .then(function(){
        user = firebase.auth().currentUser
        console.log("Login success");
        //console.log(user)
        res.send(user)
    })
    .catch(function(error) {
        // Handle Errors here.
        console.log("Login fail");
        console.log(error.code);
        console.log(error.message);
        res.send(user)
        // ...
    });
    
}

exports.signout=function(req,res,next){
    var result={}
    firebase.auth().signOut().then(function() {
        // Sign-out successful.
        console.log("signout success")
        result = {"result":"success"}
        res.send(result)
    }).catch(function(error) {
        // An error happened.
        console.log("signout fail")
        result = {"result":"fail","error":error}
        res.send(result)
    });
}

