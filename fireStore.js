

var firebase = require('firebase');
require("firebase/app");
require("firebase/auth");
require("firebase/database");
require("firebase/firestore");
require("firebase/messaging");
var functions = require("firebase/functions");

var config = {
    apiKey: "AIzaSyCbhlJSbPwSuatD5UNsA8nqpU42oe0OiMA",
    authDomain: "testjs-1f4cd.firebaseapp.com",
    databaseURL: "https://testjs-1f4cd.firebaseio.com",
    projectId: "testjs-1f4cd",
    storageBucket: "testjs-1f4cd.appspot.com",
    messagingSenderId: "227540708882"
  };
firebase.initializeApp(config);

var db=firebase.firestore()

exports.getDB=function(){
    return db
}

exports.getFirebase=function(){
    return firebase
}

