
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
var GOOGLE_BOOKS_API="https://www.googleapis.com/books/v1/volumes"
var db = require("./fireStore").getDB()
var firebase = require("./fireStore").getFirebase()
var hasCurrentUser = require("./users").hasCurrentUser

//server.get("/books/:id",getBookById)----------------------------------
exports.getBookById=function(req,res,next){
    
    var xhr=new XMLHttpRequest()
    xhr.open("GET",GOOGLE_BOOKS_API+"/"+req.params.id)
    xhr.setRequestHeader('Content-Type','json')
    xhr.onload = function (e) {
		if (xhr.readyState === 4 && xhr.status === 200) {
	    	var data = JSON.parse(xhr.responseText);
	    	var book ={
	    	    "id":data.id,
	    	    "volumeInfo":data.volumeInfo
	    	}
			res.send(book)
		}
		else {
			console.error(xhr.statusText);
		}
    }
	xhr.send()
	
}



//server.get("/books",getBooks)---------------------------------------
exports.getBooks=function(req,res,next){
    
    var xhr=new XMLHttpRequest()
    
    var keyword = (req.query.keyword == "") ? "a": req.query.keyword
    xhr.open("GET",GOOGLE_BOOKS_API+"?q="+keyword)
    xhr.setRequestHeader('Content-Type','json')
    xhr.onload = function (e) {
		if (xhr.readyState === 4 && xhr.status === 200) {
	    	var data = JSON.parse(xhr.responseText);
			var books =[]
			for(var i=0; i < data.items.length;i++){
			    console.log(data.items[i])
			    var book={
			        "id":data.items[i].id,
	    	        "volumeInfo":data.items[i].volumeInfo
			    }
			    books.push(book)
			}
			res.send(books)
		}
		else {
			console.error(xhr.statusText);
		}
    }
	xhr.send()
}

//server.post("/books",books.borrowBook)
exports.borrowBook=function(req,res,next){
	var user = firebase.auth().currentUser
    var bookId = req.body.bookId
    var result={}

	if(!hasCurrentUser(user,res)) return
    
    var recordsDoc = db.collection("records").doc(bookId)
    
    recordsDoc.get().then(function(doc){
    	if(doc.exists && doc.data().deadline > Date.now()){
    		result = {"result":"borrowed"}
        	res.send(result)
    	}
    	else{
    		db.collection("records").doc(bookId).set({
		    	"userId":user.uid,
		    	"bookId":bookId,
		    	"borrowTime":Date.now(),
		    	"deadline":Date.now()+(1000*60*60*24*7)
		    }).then(function(){
		        result = {"result":"success"}
		        res.send(result)
		    }).catch(function(error){
		        result = {"result":"fail","error":error}
		        res.send(result)
		    })
    	}
    })
    
    
}
