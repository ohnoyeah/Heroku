var restify = require('restify')

var server = restify.createServer()

server.use(restify.plugins.queryParser())
server.use(restify.plugins.bodyParser({mapParams:false}))


//server.opts('/\.*/', function(req, res,next){
 /*   res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version, X-Response-Time, X-PINGOTHER, X-CSRF-Token,Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE,OPTIONS');
    res.setHeader('Access-Control-Expose-Headers', 'X-Api-Version, X-Request-Id, X-Response-Time');
    res.setHeader('Access-Control-Max-Age', '1000');

return next();
})*/

server.pre(restify.CORS({
    origins:['*'],
    credentials:true,
    methods:['GET','POST','DELETE','PUT','OPTIONS']
}));

/*const corsMiddleware = require('restify-cors-middleware')

const cors = corsMiddleware({
  preflightMaxAge: 5, //Optional
  origins: ['https://heroku-newindex.c9users.io'],
  allowHeaders: ['*','Token'],
  exposeHeaders: ['*','Token']
})

server.pre(cors.preflight)
server.use(cors.actual)*/


server.listen(process.env.PORT || 8080, function(){
    console.log("Server created")
})


var login = require("./login")
var users = require("./users")
var books = require("./books")
var bookmark = require("./bookmark")

server.post("/users/login",login.validate)
server.post("/users/register", users.register)
server.get("/users",users.getCurrentUser)
server.get("/users/logout",login.signout)
server.get("/books/:id",books.getBookById)
server.get("/books",books.getBooks)
server.post("/books",books.borrowBook)
server.get("/bookmark",bookmark.getBookmark)
server.post("/bookmark",bookmark.addBookmark)
server.del("/bookmark",bookmark.removeBookmark)






