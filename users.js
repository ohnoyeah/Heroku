var db=require("./fireStore").getDB()
var firebase = require("./fireStore").getFirebase()



//server.post("/users/register", register)------------------------------------
exports.register=function(req,res,next){
    //if(req.headers["content-type"]!="application/json"){
    //    console.log("body not json formatted")
    //    return next(new restify.BadRequestError("body not json formatted"))
    //}
    var email = req.body.email
    var name = req.body.name
    var password = req.body.password
    var result={}
    
    /*
    var userDocRef = db.collection("users").doc(email)
    userDocRef.get().then(function(doc){
        if(!doc.exists){
            userDocRef.set({
                "email":email,
                "name":name,
                "password":password
            })
            result={"result":"success","data":doc.data()}
        }
        else{
            result={"result":"fail","data":"email has been used"}
        }
        res.send(result)
    })
    */
    firebase.auth().createUserWithEmailAndPassword(email, password)
    .then(function(){
        var user = firebase.auth().currentUser
        user.updateProfile({
            displayName : name
        }).then(function(){
            result={"result":"success","user":user}
            res.send(result)
        })
        
    })
    .catch(function(error) {
        // Handle Errors here.
        console.log(error.code);
        console.log(error.message);
        result={"result":"fail","data":"email has been used"}
        res.send(result)
    });
    
}

exports.hasCurrentUser = function(user,res){
    if(user==null){
        var result = {"result":"null_user"}
        res.send(result)
        return false
    }
    else{
        return true
    }
}

exports.getCurrentUser = function(req,res,next){
    res.send(firebase.auth().currentUser)
}